'''
LABORATORIO 5

La juguetería
Asuma que usted tiene una juguetería y quiere implementar un módulo de Python
llamado juguetes con el fin de mejorar la administración de inventario. Las
funciones que usted debe crear tendrán las siguientes descripciones:

Productos(A): Dada una lista A de juguetes, la función Productos debe generar una
lista de juguetes sin repetición.
Ejemplo:
A = ["Balón","Carrito","Muñeca","Dominó","Balón","Muñeca","Corneta","Avión",
"Carrito"]
La función debe retornar la siguiente lista: ["Balón","Carrito","Muñeca","Dominó",
"Corneta","Avión"]

Faltante (L, M, N): Dada una lista L con las posiciones de los juguetes que
faltan en el inventario, otra lista M con juguetes y un juguete N, la función
Faltante debe generar la lista de juguetes faltantes.
Ejemplo:
 L = [2, 0, 5, 1, 3] M = ["Dominó","Balón","Muñeca","Balón","Corneta","Avión"]
N = "Balón"
Se debe retornar la sublista [1, 3], tomada de la lista L, ya que justo en esas
posiciones de la lista M hay un balón.

TeFaltan (L1, L2): Para poder realizar un listado de juguetes que puedo venderle
a otra tienda de juguetes, la función TeFaltan recibe dos listas, la primera con
la lista de juguetes que yo tengo y la segunda con la lista de juguetes que
tienen en la otra tienda, y se debe generar un listado con los juguetes que le
podría vender a la otra tienda.
Ejemplo:
si se tiene como entrada:
L1 = ["Balón","Carrito","Muñeca","Dominó","Corneta","Avión","Oso"]
L2 = ["Muñeca","Perrito","Oso","Balón","Avión"]
La función debe retornar la siguiente lista: ["Carrito","Dominó","Corneta"]

Intercambiemos (L1, L2): se desea saber cuántos juguetes, máximo, se pueden
intercambiar entre otra juguetería y su juguetería. Para esto se debe implementar
la función Intercambiemos la cual recibe dos listas.
Ejemplo:
L1 = ["Balón","Carrito","Muñeca","Dominó","Corneta","Avión","Oso","Cartas"]
L2 = ["Nintendo","Muñeca","Perrito","Oso","Balón","Avión","Flechas"]
Al evaluar faltantes entre estas dos listas se encuentra que a L1 le hacen falta
4 juguetes que tiene L2 y a L2 le hacen falta 3 juguetes que L1 tiene, luego solo
se podrán intercambiar 3 juguetes entre ellos, es decir una tienda le entrega 3
juguetes a la otra tienda a cambio de otros 3 juguetes.

Entrada: Este programa no requiere entrada, ni generará salida. Se requiere que
el estudiante genere un archivo con el nombre juguetes.py y que definan los
nombres de las funciones dadas y sus parámetros tal cual está especificado en el
enunciado.

Puntuación: Si el departamento de nómina puede importar el módulo se tiene un
punto. Por cada función que cumpla con lo establecido se tiene un punto, para un
total de 5 puntos. Tenga en cuenta que la funciones no usan tildes y que hay que
respetar las mayúscula y las mínúsculas.
'''
# Function by Juan Jose Monsalve
def Productos(A):
    sin_repeticion = list()
    for i in A:
        if i not in A:
            sin_repeticion += [i]
    return sin_repeticion
    
# Function by Miguel Barragan
def Faltante(L, M, N):
    F=[x for x, i in enumerate(M)if i==N]
    C=[]
    [C.append(i) for i in L if i in F]
    return C

# Milton Mejia

def Intercambiemos (L1,L2):
    Faltan2=TeFaltan(L1,L2)
    Faltan1=TeFaltan(L2,L1)
    if len(Faltan2)==len(Faltan1):
        num=len(Faltan2)
    elif len(Faltan2)>len(Faltan1):
        num=len(Faltan1)
    else:
        num=len(Faltan2)
    return (num)    

#Juliana Cubillos
def TeFaltan(L1,L2):
    L3=L1+L2
    L3=Productos(L3)
    for j in L2:
        L3.remove (j)
    return (L3)    
# next person
# next function to realice
